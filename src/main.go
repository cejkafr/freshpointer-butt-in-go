package main

import (
	"database/sql"
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"mrpeewit.freshpointer/repository"
	"mrpeewit.freshpointer/scheduler"
	"net/http"
	"os"
	"strconv"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

type Handler struct {
	Category interface {
		GetAll() ([]repository.Category, error)
	}

	FreshPointer interface {
		GetAll() ([]repository.FreshPoint, error)
		GetByID(id int64) (*repository.FreshPoint, error)
	}

	FreshPointerProduct interface {
		Filter(freshPointId int64, filter *repository.Filter) ([]repository.FreshPointProduct, error)
		GetByID(freshPointId int64, productId int64) (repository.FreshPointProduct, error)
	}
}

func initDB() *sql.DB {
	host := os.Getenv("DB_HOST")
	port := os.Getenv("DB_PORT")
	if port == "" {
		port = "3306"
	}
	name := os.Getenv("DB_NAME")
	username := os.Getenv("DB_USERNAME")
	password := os.Getenv("DB_PASSWORD")

	//log.Println(fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", username, password, host, port, name))
	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", username, password, host, port, name))

	if err != nil {
		panic(err)
	}

	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)
	db.Ping()

	return db
}

func initRouter(h *Handler) *gin.Engine {
	router := gin.Default()
	router.GET("/api/v1/categories", h.getCategories)
	router.GET("/api/v1/freshpoints", h.getFreshPoints)
	router.GET("/api/v1/freshpoints/:freshPointId", h.getFreshPointById)
	router.GET("/api/v1/:freshPointId/products", h.getFreshPointProducts)
	router.GET("/api/v1/:freshPointId/products/:productId", h.getFreshPointProductById)
	router.GET("/api/v1/:freshPointId/products/:productId/history-amount", h.getCategories)
	router.GET("/api/v1/:freshPointId/products/:productId/history-price", h.getCategories)

	//router.Static("/", "./static")

	return router
}

func main() {
	db := initDB()
	defer db.Close()

	scraper := &scheduler.ScrapeScheduler{DB: db, Timeout: time.Second * 60}
	go scraper.Execute()

	h := &Handler{
		Category:            repository.CategoryRepo{DB: db},
		FreshPointer:        repository.FreshPointRepo{DB: db},
		FreshPointerProduct: repository.FreshPointProductRepo{DB: db},
	}

	httpPort := os.Getenv("HTTP_PORT")
	if httpPort == "" {
		httpPort = "8080"
	}

	router := initRouter(h)
	router.Run(":" + httpPort)
}

func (ct Handler) getCategories(c *gin.Context) {
	res, err := ct.Category.GetAll()

	if err != nil {
		log.Println(err)
		c.Status(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, res)
}

func (ct Handler) getFreshPoints(c *gin.Context) {
	res, err := ct.FreshPointer.GetAll()
	if err != nil {
		log.Print(err)
		c.Status(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, res)
}

func (ct Handler) getFreshPointById(c *gin.Context) {
	freshPointId := strToInt64(c.Param("freshPointId"))

	res, err := ct.FreshPointer.GetByID(freshPointId)

	if err != nil {
		log.Print(err)
		c.Status(http.StatusInternalServerError)
		return
	}

	if res == nil {
		c.Status(http.StatusNotFound)
		return
	}

	c.JSON(http.StatusOK, res)
}

func (ct Handler) getFreshPointProducts(c *gin.Context) {
	freshPointId := strToInt64(c.Param("freshPointId"))
	filter := new(repository.Filter)
	filter.Timestamp = c.Query("timestamp")
	for _, v := range c.QueryArray("productIds") {
		filter.ProductIDs = append(filter.ProductIDs, strToInt64(v))
	}

	res, err := ct.FreshPointerProduct.Filter(freshPointId, filter)

	if err != nil {
		log.Print(err)
		c.Status(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, res)
}

func (ct Handler) getFreshPointProductById(c *gin.Context) {
	freshPointId := strToInt64(c.Param("freshPointId"))
	productId := strToInt64(c.Param("productId"))

	res, err := ct.FreshPointerProduct.GetByID(freshPointId, productId)

	if err != nil {
		log.Print(err)
		c.Status(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, res)
}

func strToInt64(s string) int64 {
	i, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}
