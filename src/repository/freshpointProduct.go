package repository

import (
	"database/sql"
)

type Filter struct {
	Timestamp  string
	ProductIDs []int64
}

type FreshPointProduct struct {
	ID           int64   `json:"id"`
	Amount       int     `json:"amount"`
	Price        float64 `json:"price"`
	Discount     bool    `json:"discount"`
	ProductId    int64   `json:"productId"`
	CategoryId   int64   `json:"categoryId"`
	Name         string  `json:"name"`
	Manufacturer string  `json:"manufacturer"`
	PackageSize  struct {
		Value int    `json:"value"`
		Unit  string `json:"unit"`
	} `json:"packageSize"`
	URL            string `json:"url"`
	CreatedAt      string `json:"createdAt"`
	UpdatedAt      string `json:"updatedAt"`
	LastResupplyAt string `json:"lastResupplyAt"`
	LastDiscountAt string `json:"lastDiscountAt"`
}

type FreshPointProductHistory struct {
	ProductId int64   `json:"productId"`
	Amount    int     `json:"amount"`
	Price     float64 `json:"price"`
	Delta     float64 `json:"delta"`
	CreatedAt string  `json:"createdAt"`
}

type FreshPointProductRepo struct {
	DB *sql.DB
}

func (r FreshPointProductRepo) Filter(freshPointId int64, filter *Filter) ([]FreshPointProduct, error) {
	q := "select fp.id, fp.amount, fp.price, fp.discount, fp.product_id, p.category_id, p.name, p.manufacturer, p.package_size, p.package_unit, p.image, fp.created_at, fp.updated_at from freshpoint_product fp join product p on p.id = fp.product_id join freshpoint f on f.id = fp.freshpoint_id where f.external_id = ?"
	if len(filter.Timestamp) > 0 {
		q += " and updated_at > ?"
	}
	if len(filter.ProductIDs) > 0 {
		q += " and fp.product_id in (?)"
	}
	res, err := r.DB.Query(q, freshPointId)
	defer res.Close()

	if err != nil {
		return nil, err
	}

	var arr []FreshPointProduct

	for res.Next() {
		var dto FreshPointProduct

		err := res.Scan(&dto.ID, &dto.Amount, &dto.Price, &dto.Discount, &dto.ProductId, &dto.CategoryId, &dto.Name,
			&dto.Manufacturer, &dto.PackageSize.Value, &dto.PackageSize.Unit, &dto.URL, &dto.CreatedAt, &dto.UpdatedAt)
		if err != nil {
			return nil, err
		}

		arr = append(arr, dto)
	}

	return arr, nil
}

func (r FreshPointProductRepo) GetByID(freshPointId int64, productId int64) (FreshPointProduct, error) {
	res := r.DB.QueryRow("select fp.id, fp.amount, fp.price, fp.discount, fp.product_id, p.category_id, p.name, p.manufacturer, p.package_size, p.package_unit, p.image, fp.created_at, fp.updated_at from freshpoint_product fp join product p on p.id = fp.product_id join freshpoint f on f.id = fp.freshpoint_id where f.external_id = ? and fp.product_id = ?", freshPointId, productId)

	if res.Err() != nil {
		return FreshPointProduct{}, res.Err()
	}

	var dto FreshPointProduct

	err := res.Scan(&dto.ID, &dto.Amount, &dto.Price, &dto.Discount, &dto.ProductId, &dto.CategoryId, &dto.Name, &dto.Manufacturer, &dto.PackageSize.Value, &dto.PackageSize.Unit, &dto.URL, &dto.CreatedAt, &dto.UpdatedAt)
	if err != nil {
		return FreshPointProduct{}, err
	}

	return dto, nil
}
