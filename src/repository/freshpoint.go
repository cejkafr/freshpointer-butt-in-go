package repository

import (
	"database/sql"
)

type FreshPoint struct {
	ID             int64  `json:"id"`
	Name           string `json:"name"`
	LastResupplyAt string `json:"lastResupplyAt"`
}

type FreshPointRepo struct {
	DB *sql.DB
}

func (r FreshPointRepo) GetAll() ([]FreshPoint, error) {
	res, err := r.DB.Query("select id, name, updated_at from freshpoint")
	defer res.Close()

	if err != nil {
		return nil, err
	}

	var arr []FreshPoint

	for res.Next() {
		var dto FreshPoint

		err := res.Scan(&dto.ID, &dto.Name, &dto.LastResupplyAt)
		if err != nil {
			return nil, err
		}

		arr = append(arr, dto)
	}

	return arr, nil
}

func (r FreshPointRepo) GetByID(id int64) (*FreshPoint, error) {
	res := r.DB.QueryRow("select id, name, updated_at from freshpoint where id = ?", id)

	if res.Err() != nil {
		return nil, res.Err()
	}

	var dto FreshPoint

	err := res.Scan(&dto.ID, &dto.Name, &dto.LastResupplyAt)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		} else {
			return nil, err
		}
	}

	return &dto, nil
}
