package repository

import (
	"database/sql"
)

type Category struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

type CategoryRepo struct {
	DB *sql.DB
}

func (r CategoryRepo) GetAll() ([]Category, error) {
	res, err := r.DB.Query("select id, name from category")
	defer res.Close()

	if err != nil {
		return nil, err
	}

	var arr []Category

	for res.Next() {
		var dto Category
		err := res.Scan(&dto.ID, &dto.Name)

		if err != nil {
			return nil, err
		}

		arr = append(arr, dto)
	}

	return arr, nil
}
