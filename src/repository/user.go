package repository

import (
	"database/sql"
)

type User struct {
	ID             int64  `json:"id"`
	Name           string `json:"name"`
	LastResupplyAt string `json:"lastResupplyAt"`
}

type UserRepo struct {
	DB *sql.DB
}

func (r UserRepo) GetAll() ([]User, error) {
	res, err := r.DB.Query("select id, name, updated_at from freshpoint")
	defer res.Close()

	if err != nil {
		return nil, err
	}

	var arr []User

	for res.Next() {
		var dto User

		err := res.Scan(&dto.ID, &dto.Name, &dto.LastResupplyAt)
		if err != nil {
			return nil, err
		}

		arr = append(arr, dto)
	}

	return arr, nil
}

func (r UserRepo) GetByID(id int64) (User, error) {
	res := r.DB.QueryRow("select id, name, updated_at from freshpoint where id = ?", id)

	if res.Err() != nil {
		return User{}, res.Err()
	}

	var dto User

	err := res.Scan(&dto.ID, &dto.Name, &dto.LastResupplyAt)
	if err != nil {
		return User{}, err
	}

	return dto, nil
}
