package scheduler

import (
	"database/sql"
	"fmt"
	"time"
)

type ScrapeScheduler struct {
	DB      *sql.DB
	Timeout time.Duration
}

func (s ScrapeScheduler) Execute() {
	for true {
		// TODO
		fmt.Println("Executing scheduler.")
		time.Sleep(s.Timeout)
	}
}
