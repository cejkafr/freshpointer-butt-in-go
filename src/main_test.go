package main

import (
	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
	"io"
	"mrpeewit.freshpointer/repository"
	"net/http"
	"net/http/httptest"
	"testing"
)

type MockCategoryRepo struct {
}

type MockFreshPointRepo struct {
}

type MockFreshPointProductRepo struct {
}

func (r MockCategoryRepo) GetAll() ([]repository.Category, error) {
	var data []repository.Category
	data = append(data, repository.Category{ID: 1, Name: "Test"})
	data = append(data, repository.Category{ID: 2, Name: "Test 1"})
	return data, nil
}

func (r MockFreshPointRepo) GetAll() ([]repository.FreshPoint, error) {
	var data []repository.FreshPoint
	data = append(data, repository.FreshPoint{ID: 1, Name: "Test"})
	data = append(data, repository.FreshPoint{ID: 2, Name: "Test 1"})
	return data, nil
}

func (r MockFreshPointRepo) GetByID(id int64) (*repository.FreshPoint, error) {
	return &repository.FreshPoint{ID: id, Name: "Test"}, nil
}

func (r MockFreshPointProductRepo) Filter(freshPointId int64, filter *repository.Filter) ([]repository.FreshPointProduct, error) {
	var data []repository.FreshPointProduct
	data = append(data, repository.FreshPointProduct{ID: 1, Name: "Test"})
	data = append(data, repository.FreshPointProduct{ID: 2, Name: "Test 1"})
	return data, nil
}

func (r MockFreshPointProductRepo) GetByID(freshPointId int64, productId int64) (repository.FreshPointProduct, error) {
	return repository.FreshPointProduct{ID: productId, Name: "Test"}, nil
}

func TestGetCategories(t *testing.T) {
	h := &Handler{
		Category:            MockCategoryRepo{},
		FreshPointer:        MockFreshPointRepo{},
		FreshPointerProduct: MockFreshPointProductRepo{},
	}

	r := gin.Default()
	r.GET("/api/v1/categories", h.getCategories)

	rec := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/v1/categories", nil)
	r.ServeHTTP(rec, req)

	responseData, _ := io.ReadAll(rec.Body)
	assert.Equal(t, http.StatusOK, rec.Code)
	assert.Equal(t, "[{\"id\":1,\"name\":\"Test\"},{\"id\":2,\"name\":\"Test 1\"}]", string(responseData))
}

func TestGetFreshPoints(t *testing.T) {
	h := &Handler{
		Category:            MockCategoryRepo{},
		FreshPointer:        MockFreshPointRepo{},
		FreshPointerProduct: MockFreshPointProductRepo{},
	}

	r := gin.Default()
	r.GET("/api/v1/freshpoints", h.getFreshPoints)

	rec := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/v1/freshpoints", nil)
	r.ServeHTTP(rec, req)

	responseData, _ := io.ReadAll(rec.Body)
	assert.Equal(t, http.StatusOK, rec.Code)
	assert.Equal(t, "[{\"id\":1,\"name\":\"Test\",\"lastResupplyAt\":\"\"},{\"id\":2,\"name\":\"Test 1\",\"lastResupplyAt\":\"\"}]", string(responseData))
}
